from discord.ext import commands
from discord.ext.commands import Cog
import discord
import requests
import json
import datetime

class reddit(Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def reddit(self, ctx, user):
        """Lookup a user on reddit"""
        if user[0:2] == "u/":
            user = user[2:len(user)]
        r = requests.get(url = f"https://reddit.com/user/{user}/about.json", headers = {'User-agent': 'Gralyn Core'}).content
        data = json.loads(r)
        if "error" not in data:
            data["error"] = None
        if data["error"] == 404:
            await ctx.send("That user does not exist!")
        else:
            user = data["data"]
            embed = discord.Embed(title=f"u/{user['name']}", url=f"https://reddit.com/u/{user['name']}")
            embed.set_thumbnail(url=user["icon_img"])
            embed.add_field(name="Total Karma", value=user["link_karma"]+user["comment_karma"], inline=True)
            embed.add_field(name="Link karma", value=user["link_karma"], inline=True)
            embed.add_field(name="comment karma", value=user["comment_karma"], inline=True)
            embed.add_field(name="Created at", value=datetime.datetime.utcfromtimestamp(user["created"]).strftime("%Y-%m-%d %H:%M"), inline=True)
            embed.add_field(name="Gold", value=user["is_gold"], inline=True)
            embed.add_field(name="Verified Email", value=user["verified"], inline=True)
            embed.add_field(name="Employee", value=user["is_employee"], inline=True)
            await ctx.send(embed=embed)
    
def setup(bot):
    bot.add_cog(reddit(bot))