from discord.ext import commands
from discord.ext.commands import Cog
import discord
import requests
import json

class urban(Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def urban(self, ctx, *args):
        """Lookup a word in the urban dictionary"""
        args = ' '.join(args)
        response_raw = requests.get(f"http://api.urbandictionary.com/v0/define?term={args}").content
        response = json.loads(response_raw)
        if len(response["list"]) == 0:
            await ctx.send("No results found")
        else:
            word = response["list"][0]
            output = ""
            for x in word['definition']:
                if x is not "[" and x is not "]":
                    output = output + x
            await ctx.send(output)

def setup(bot):
    bot.add_cog(urban(bot))