from discord.ext import commands
from discord.ext.commands import Cog
import discord

class gradyn(commands.Cog):
    def __init__(self, bot):
        self.bot = bot 
    @Cog.listener()
    async def on_raw_reaction_add(self, payload):
        if payload.channel_id != 519924821769977856:
            if payload.emoji.name == "⭐":
                if payload.guild_id == 404835018922262532:
                    channel = self.bot.get_channel(payload.channel_id)
                    message = await channel.get_message(payload.message_id)
                    if message.author.id == payload.user_id:
                        await channel.send("You cannot star your own message!")
                    else:
                        reacted = False
                        for reaction in message.reactions:
                            if reaction.emoji == "⭐":
                                if reaction.count > 1:
                                    reacted = True
                                    break
                        if not reacted:
                            embed = discord.Embed(description=message.content)
                            embed.set_author(name=message.author.display_name, icon_url=message.author.avatar_url)
                            embed.set_footer(text=message.created_at)
                            channel = self.bot.get_channel(519924821769977856)
                            await channel.send(embed=embed)
        if payload.message_id == 503755610014220298:
            guild = self.bot.get_guild(404835018922262532)
            member = guild.get_member(payload.user_id)
            for x in guild.channels:
                if x.name == "information":
                    message = await x.get_message(503755610014220298)
                    await message.remove_reaction(payload.emoji.name, member)
                    break
            await message.remove_reaction(payload.emoji.name, member)
            if payload.emoji.name == "🇦":
                role = discord.utils.get(member.guild.roles, name="Artists")
                if role not in member.roles:
                    await member.add_roles(role)
                else:
                    await member.remove_roles(role)
            if payload.emoji.name == "🇬":
                role = discord.utils.get(member.guild.roles, name="Gamers")
                if role not in member.roles:
                    await member.add_roles(role)
                else:
                    await member.remove_roles(role)
            if payload.emoji.name == "🇹":
                role = discord.utils.get(member.guild.roles, name="Techies")
                if role not in member.roles:
                    await member.add_roles(role)
                else:
                    await member.remove_roles(role)
        if payload.message_id == 517199713603158031:
            guild = self.bot.get_guild(404835018922262532)
            member = guild.get_member(payload.user_id)
            for x in guild.channels:
                if x.name == "confirmation":
                    message = await x.get_message(517199713603158031)
                    await message.remove_reaction(payload.emoji.name, member)
                    break
            if payload.emoji.name == "🇷":
                await member.send("Accidentally clicked 🇷? Don't worry, here's a link to rejoin the server! <http://nebula.gradyn.com>")
                await member.kick()
            if payload.emoji.name == "🇻":
                role = discord.utils.get(member.guild.roles, name="Verified")
                if role not in member.roles:
                    await member.add_roles(role)
    async def on_member_join(self, member):
        if member.guild.id == 405235135013978113:
            role = discord.utils.get(member.guild.roles, name="General Public")
            await member.add_roles(role)
            intros = self.bot.get_channel(508772695949836288)
            await intros.send(f"Welcome to Void Hub, <@{member.id}>! Please read the <#406256366467153920> and <#508774572892618772> and introduce yourself in <#508772695949836288>. Enjoy your stay!")
        if member.guild.id == 404835018922262532:
            intros = self.bot.get_channel(511192107482873866)
            await intros.send(f"Welcome to Nebula, <@{member.id}>! I'm gralyn, the guilds robot assistant. Please read the rules and info in <#404836126646337576> before continuing. You may also assign yourself roles there. Enjoy your stay!")
    @commands.command()
    async def E(self, ctx):
        """E meme best meme"""
        await ctx.send("""```
EEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEE
  EEEEEEE       EEEEEE
  EEEEEEE             
  EEEEEEEEEEEEEEEEE   
  EEEEEEEEEEEEEEEEE   
  EEEEEEEEEEEEEEEEE   
  EEEEEEEEEEEEEEEEE   
  EEEEEEE             
  EEEEEEE       EEEEEE
EEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEE```""")
    @commands.command()
    async def earth(self, ctx):
        embed = discord.Embed()
        embed.set_image(url="https://cdn.discordapp.com/attachments/496014450172755998/510313387406655489/image0.png")
        await ctx.send(embed=embed)
def setup(bot):
    bot.add_cog(gradyn(bot))
