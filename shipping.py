from discord.ext import commands
import requests 

class shipping(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def ups(self, ctx, tracking):
        payload = {"Locale": "en_US", "TrackingNumber":[tracking]}
        response = requests.post('https://www.ups.com/track/api/Track/GetStatus?loc=en_US', json=payload).json()
        if response["statusCode"] == "200":
            details = response["trackDetails"][0]
            final = f"{details['trackingNumber']} (Scheduled for {details['scheduledDeliveryDate']}):\n **{details['packageStatus']}**\n"
            for x in reversed(details["shipmentProgressActivities"]):
                if x['activityScan'] != None:
                    final = final + f"{x['date']} {x['time']}` {x['activityScan']}` in `{x['location']}`\n"
            await ctx.send(final)
        else:
            await ctx.send("Something went wrong! Check your tracking number")

def setup(bot):
    bot.add_cog(shipping(bot))
    