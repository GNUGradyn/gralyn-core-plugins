from discord.ext import commands
import discord
import json
import requests
import dns.resolver

resolver = dns.resolver.Resolver()
ids = ["A","AAAA","CNAME","MX","TXT"]
class Hack(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def ipinfo(self, ctx, ip=None):
        """Gets info about an IP"""
        if ip == None:
            await ctx.send("Please provide an IP!")
        else:
            ipinfo_raw = requests.get(f"https://ipinfo.io/{ip}").content
            ipinfo = json.loads(ipinfo_raw)
            if "error" in ipinfo:
                await ctx.send("An error occured looking up that ip - Please check the IP")
            else:
                embed = discord.Embed(title=ip)
                if "bogon" in ipinfo:
                    embed.add_field(name="Bogon", value="True", inline=True)
                if "hostname" in ipinfo and ipinfo["hostname"] != "":
                    embed.add_field(name="hostname", value=ipinfo["hostname"], inline=True)
                if "city" in ipinfo and ipinfo["city"] != "":
                    embed.add_field(name="city", value=ipinfo["city"], inline=True)
                if "region" in ipinfo and ipinfo["region"] != "":
                    embed.add_field(name="region", value=ipinfo["region"], inline=True)
                if "loc" in ipinfo and ipinfo["loc"] != "":
                    embed.add_field(name="Location", value=ipinfo["loc"], inline=True)
                if "postal" in ipinfo and ipinfo["postal"] != "":
                    embed.add_field(name="ZIP code", value=ipinfo["postal"], inline=True)
                if "org" in ipinfo and ipinfo["org"] != "":
                    embed.add_field(name="Organization", value=ipinfo["org"], inline=True)
                await ctx.send(embed=embed)
    @commands.command()
    async def dns(self, ctx, domain=None):
        """Shows all the DNS records a domain has"""
        try:
            embed = discord.Embed(title=domain)
            for a in ids:
                try:
                    records = dns.resolver.query(domain, a)
                    for record in records:
                        embed.add_field(name=a, value=record)
                except dns.resolver.NoAnswer:
                    pass
            await ctx.send(embed=embed)
        except dns.resolver.NXDOMAIN:
            await ctx.send("Domain does not exist! Check your spelling.")

def setup(bot):
    bot.add_cog(Hack(bot))