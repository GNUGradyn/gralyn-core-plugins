from discord.ext import commands
import requests 

class wikipedia(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def wikipedia(self, ctx, *query):
        """search wikipedia"""
        query = " ".join(query)
        payload = {'action':'query','list':'search','srsearch':query,'format':'json'}
        result = requests.get('https://en.wikipedia.org/w/api.php', params=payload).json()
        try:
            await ctx.send(result["query"]["search"][0]["snippet"].replace('<span class="searchmatch">',"").replace("</span>","").replace("&quot;","**"))
        except IndexError:
            await ctx.send("No results found on wikipedia!")
def setup(bot):
    bot.add_cog(wikipedia(bot))