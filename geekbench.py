from discord.ext import commands
import discord
import json
import requests

cpu_benchmarks_raw = requests.get("https://browser.geekbench.com/processor-benchmarks.json").content
cpu_benchmarks = json.loads(cpu_benchmarks_raw)
cpu_benchmarks_raw = None
gpu_benchmarks_raw = requests.get("https://browser.geekbench.com/opencl-benchmarks.json").content
gpu_benchmarks = json.loads(gpu_benchmarks_raw)
gpu_benchmarks_raw = None

class geekbench(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def cpu(self, ctx, *cpu):
        """Look up a CPU in geekbench"""
        if cpu is None:
            await ctx.send("Enter a CPU to look up!")
        cpu = ' '.join(cpu)
        for device in cpu_benchmarks["devices"]:
            found = False
            if cpu.lower() in device["name"].lower():
                found = True
                embed = discord.Embed(title=device["name"], description=f"{device['description']} | {device['family']}", url=f"https://browser.geekbench.com/processors/{device['id']}")
                embed.add_field(name="Single core", value=device["score"])
                embed.add_field(name="Multi core", value=device["multicore_score"])
                embed.set_footer(text="Geekbench results", icon_url="https://www.geekbench.com/img/hogtown/windows.png")
                await ctx.send(embed=embed)
                break
        if not found:
            await ctx.send("No results found in geekbench database!")
    @commands.command()
    async def gpu(self, ctx, *gpu):
        """Look up a GPU in geekbench"""
        if gpu is None:
            await ctx.send("Enter a GPU to look up!")
        gpu = ' '.join(gpu)
        for device in gpu_benchmarks["devices"]:
            found = False
            if gpu.lower() in device["name"].lower():
                found = True
                embed = discord.Embed(title=device["name"])
                embed.add_field(name="Score", value=device["score"])
                embed.set_footer(text="Geekbench results", icon_url="https://www.geekbench.com/img/hogtown/windows.png")
                await ctx.send(embed=embed)
                break
        if not found:
            await ctx.send("No results found in geekbench database!")
def setup(bot):
    bot.add_cog(geekbench(bot))