from discord.ext import commands
from discord.ext.commands import Cog
import discord
import json
import requests

class mcstat(Cog):
    def __init__(self, bot):
        self.bot = bot
        if "mcstat_default" not in self.bot.config:
            self.bot.config["mcstat_default"] = "" 
    @commands.command()
    async def mcstat(self, ctx, *address):
        if len(address) != 0:
            address = address[0]
        else: 
            if self.bot.config["mcstat_default"] != "":
                address = self.bot.config["mcstat_default"]
            else:
                address = ""
                await ctx.send("No address specified and no default set, please specify an address or set a default")
        if address != "":
            data_raw = requests.get("https://mcapi.us/server/status", params={"ip":address}).text
            data = json.loads(data_raw)
            if data["status"] == "error":
                await ctx.send("Something went wrong! Check your address")
            else:
                if data["online"]:
                    embed = discord.Embed(title=address)
                    embed.add_field(name="Status", value="Online")
                    embed.add_field(name="Players online", value=f"{data['players']['now']}/{data['players']['max']}")
                    embed.add_field(name="motd", value=data["motd"])
                    await ctx.send(embed=embed)
                else:
                    await ctx.send(f"Minecraft server {address} appears to be offline")
def setup(bot):
    bot.add_cog(mcstat(bot))