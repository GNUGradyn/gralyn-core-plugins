translate_times = 5

from discord.ext import commands
from discord.ext.commands import Cog
from googletrans import Translator
import random

translator = Translator()
langs = ['Afrikaans', 'Albanian', 'Amharic', 'Arabic', 'Armenian', 'Azerbaijani', 'Basque', 'Belarusian', 'Bengali', 'Bosnian', 'Bulgarian', 'Catalan', 'Cebuano', 'Chichewa', 'Corsican', 'Croatian', 'Czech', 'Danish', 'Dutch', 'English', 'Esperanto', 'Estonian', 'Filipino', 'Finnish', 'French', 'Frisian', 'Galician', 'Georgian', 'German', 'Greek', 'Gujarati', 'Haitian Creole', 'Hausa', 'Hawaiian', 'Hebrew', 'Hindi', 'Hmong', 'Hungarian', 'Icelandic', 'Igbo', 'Indonesian', 'Irish', 'Italian', 'Japanese', 'Javanese', 'Kannada', 'Kazakh', 'Khmer', 'Korean', 'Kurdish (Kurmanji)', 'Kyrgyz', 'Lao', 'Latin', 'Latvian', 'Lithuanian', 'Luxembourgish', 'Macedonian', 'Malagasy', 'Malay', 'Malayalam', 'Maltese', 'Maori', 'Marathi', 'Mongolian', 'Nepali', 'Pashto', 'Persian', 'Polish', 'Portuguese', 'Punjabi', 'Romanian', 'Russian', 'Samoan', 'Scots Gaelic', 'Serbian', 'Sesotho', 'Shona', 'Sindhi', 'Sinhala', 'Slovak', 'Slovenian', 'Somali', 'Spanish', 'Sundanese', 'Swahili', 'Swedish', 'Tajik', 'Tamil', 'Telugu', 'Thai', 'Turkish', 'Ukrainian', 'Urdu', 'Uzbek', 'Vietnamese', 'Welsh', 'Xhosa', 'Yiddish', 'Yoruba']
class translate(Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def translate(self, ctx, *args):
        """Auto translate to english"""
        args = ' '.join(args)
        translation = translator.translate(args)
        await ctx.send(f"Translation from {translation.src}: {translation.text}")
    @commands.command()
    async def translateto(self, ctx, lang, *args):
        """Translate to another language"""
        args = ' '.join(args)
        try:
            translation = translator.translate(args, dest=lang)
            await ctx.send(f"Translation to {translation.dest}: {translation.text}")
        except ValueError:
            await ctx.send(f"Language {lang} does not exist or is not supported")
    @commands.command()
    async def teletranslate(self, ctx, *args):
        """Translates a phrase 20 times, then back to english"""
        args = ' '.join(args)
        translations = []
        for x in range(0,translate_times):
            translation = translator.translate(args, dest=random.choice(langs))
            translations.append(translation)
            args = translation.text
        translations.append(translator.translate(args))
        output = ""
        for x in translations:
            output = output + f"{x.dest}: {x.text}\n"
        if len(output) > 2000:
            await ctx.send(translations[-1].text)
        else: 
            await ctx.send(output)
            
        
def setup(bot):
    bot.add_cog(translate(bot))

