from discord.ext import commands
from discord.ext.commands import Cog
import discord
import os
from colorama import init, Fore, Back, Style
import random
init()
import json
import string
def log(text):
    print(Style.BRIGHT + Fore.WHITE + '[' + Fore.GREEN + 'General Commands' + Fore.WHITE + '] ' + Style.RESET_ALL + text)
typer = dict()
def RepInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        if "dice-max" not in self.bot.config:
            self.bot.config["dice-max"] = 6
        if "8ball" not in self.bot.config:
            self.bot.config["8ball"] = ["Yes","No","Most Likely", "Certianly not", "Absolutely", "My reply is no", "Signs point to yes", "Don't count on it", "Outlook not so good", "Definitely"]
    @Cog.listener()
    async def on_ready(self):
        if "game" in self.bot.config:
            await self.bot.change_presence(activity=discord.Game(name=self.bot.config["game"]))
    @commands.command()
    async def ping(self, ctx):
        """pong!"""
        await ctx.send("Pong!")
    @commands.command()
    async def userinfo(self, ctx, user=None):
        """Get info about yourself or another user"""
        if user is None:
            user = ctx.author
        elif RepInt(user[2:-1]):
            if ctx.guild is not None:
                user = ctx.guild.get_member(int(user[2:-1]))
            else: 
                await ctx.send("You can only get your own info in DM!")
                user = None
        elif RepInt(user[3:-1]):
            user = ctx.guild.get_member(int(user[3:-1]))   
        else:
            user = None
            await ctx.send("Invalid user!")
        if user is not None:
            embed = discord.Embed(title=f"Info for {user.name}:{user.discriminator}")
            embed.set_thumbnail(url=user.avatar_url)
            embed.add_field(name="ID", value=user.id, inline=True)
            embed.add_field(name="Created at", value=user.created_at, inline=True)
            if ctx.guild is not None:
                if user.mobile_status is not discord.Status.offline:
                    embed.add_field(name="On Mobile", value="True", inline=True)
                else:
                    embed.add_field(name="On Mobile", value="False", inline=True)
                if user.nick is not None:
                    embed.add_field(name="Nick", value=user.nick, inline=True)
                embed.add_field(name="Joined", value=user.joined_at, inline=True)
                embed.add_field(name="Status", value=user.status, inline=True)
                if user.activity is not None:
                    embed.add_field(name="Game", value=user.activity.name, inline=True)
            embed.add_field(name="Bot", value=user.bot, inline=True)
            await ctx.send(embed=embed)
    @commands.command()
    async def serverinfo(self, ctx):
        """Gets info about the current server""" 
        embed = discord.Embed(title=f"Information for {ctx.guild.name}")
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.add_field(name="Owner", value=ctx.guild.owner.display_name, inline=True)
        embed.add_field(name="id", value=ctx.guild.id, inline=True)
        embed.add_field(name="members", value=ctx.guild.member_count)
        embed.add_field(name="Created", value=ctx.guild.created_at, inline=True)
        await ctx.send(embed=embed)
    @commands.command()
    async def flip(self, ctx):
        """Coin flip"""
        await ctx.send(random.choice(['I choose heads!', 'I choose tails!']))
    @commands.command(name='8ball')
    async def eight_ball(self, ctx):
        """Ask the magic 8 ball anything and the truth will be revealed"""
        await ctx.send(random.choice(ctx.bot.config["8ball"]))
    @commands.command()
    async def choose(self, ctx, *options):
        """Choose from a list of things"""
        if len(options) == 0:
            await ctx.send("You need to list some things to choose from!")
        else:
            await ctx.send(random.choice(options))
    @commands.command()
    async def invite(self, ctx):
        """Generate an invite link"""
        app_info = await self.bot.application_info()
        await ctx.send(f"https://discordapp.com/oauth2/authorize?client_id={app_info.id}&scope=bot&permissions=0")
    @commands.command()
    async def secho(self, ctx, *message):
        """Send a message as this bot"""
        if ctx.message.author.id in self.bot.config["admins"]:
            await ctx.message.delete()
            await ctx.send(" ".join(message))
        else:
            await ctx.send(self.bot.denied())
    @commands.command()
    async def game(self, ctx, *game):
        """Set what game the bot is playing, none to clear"""
        if "game" not in self.bot.config:
            log("No game key! Creating it for you..")
            self.bot.config.pop("game", None)
            self.bot.commit()
        if ctx.message.author.id in self.bot.config["admins"]:
            if ' '.join(game) == "none":
                self.bot.config.pop("game", None)
                self.bot.commit()
                await self.bot.change_presence(activity=None)
                log("Game reset")
                await ctx.message.add_reaction("✅")
            else:
                self.bot.config["game"] = ' '.join(game)
                self.bot.commit()
                await ctx.bot.change_presence(activity=discord.Game(name=self.bot.config["game"]))
                log(f"Game set to {self.bot.config['game']}")
                await ctx.message.add_reaction("✅")
        else:
            await ctx.send(self.bot.denied())
    @commands.command(name="typer")
    async def typercmd(self, ctx):
        """See who can type the fastest!"""
        if ctx.channel.id in typer:
            await ctx.send("A game is already running on this channel!")
        else:
            characters = string.ascii_letters + string.digits
            rawmessage = ""
            for x in range(0,9):
                rawmessage = rawmessage + random.choice(characters)
            typer[ctx.channel.id] = rawmessage
            message = f"{rawmessage[0:4]}\N{zero width space}{rawmessage[4:9]}"
            await ctx.send(f"Type `{ctx.bot.config['prefix']}a {message}` the fastest to win!")
    @commands.command(hidden=True)
    async def a(self, ctx, response=""):
        if ctx.channel.id in typer:
            rawmessage = typer[ctx.channel.id]
            if response == rawmessage:
                await ctx.send(f"{ctx.author.display_name} won!")
                typer.pop(ctx.channel.id, None)
            if response == f"{rawmessage[0:4]}​{rawmessage[4:9]}":
                await ctx.send(f"{ctx.author.display_name} tried to cheat!")
        else:
            await ctx.send("There is no game running on this channel!")
    @commands.command()
    async def roll(self, ctx, number=-1):
        """Chooeses a random number"""
        if number == -1:
            number = self.bot.config["dice-max"]
        await ctx.send(f"🎲 {random.randint(1,int(number))}")
    @commands.command()
    async def rps(self, ctx, choice=None):
        """A classic rock paper scissors game"""
        if choice not in ["rock","paper","scissors"]:
            await ctx.send('You must choose `rock`, `paper`, or `scissors`!')
        else:
            me = random.choice(["rock","paper","scissors"])
            if me == choice:
                await ctx.send(f"I choose {me}, it's a tie!")
            if me == "rock" and choice == "paper":
                await ctx.send("I choose rock, you win!")
            if me == "rock" and choice == "scissors":
                await ctx.send("I choose rock, I win!")
            if me == "paper" and choice == "scissors":
                await ctx.send("I choose paper, you win!")
            if me == "paper" and choice == "rock":
                await ctx.send("I choose paper, I win!")
            if me == "scissors" and choice == "paper":
                await ctx.send("I choose scissors, I win!")
            if me == "scissors" and choice == "rock":
                await ctx.send("I choose scissors, you win!")
def setup(bot):
    bot.add_cog(General(bot))
