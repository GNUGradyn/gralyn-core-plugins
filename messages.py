from discord.ext import commands
from discord.ext.commands import Cog
from datetime import datetime
file = open("messages.txt","a+")

class messages(Cog):
    def __init__(self, bot):
        self.bot = bot
    @Cog.listener()
    async def on_message(self, message):
        if message.channel.id == 318849191675953152:
            file.write(f"{datetime.now().strftime('%Y-%m-%d %H:%M')} {message.author.name}({message.author.id}): {message.content}\n")
            file.flush()

def setup(bot):
    bot.add_cog(messages(bot))
