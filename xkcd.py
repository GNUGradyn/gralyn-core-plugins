from discord.ext import commands
import json
import requests
import discord
import random

def is_number(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
months = ["January","Febuary","March","April","May","June","July","August","September","October","November","December"]

class xkcd(commands.Cog):
    def __init__(self, bot):
       self.bot = bot
    @commands.command()
    async def xkcd(self, ctx, ID=None):
        if ID == None:
            current_xkcd_raw = requests.get(f"https://xkcd.com/info.0.json").content
            current_xkcd = json.loads(current_xkcd_raw)
            ID = random.randint(1,int(current_xkcd["num"]))
        if is_number(ID):
            xkcd_raw = requests.get(f"https://xkcd.com/{int(ID)}/info.0.json").content
            xkcd = json.loads(xkcd_raw)
            embed = discord.Embed(title=xkcd["safe_title"], url=f"https://xkcd.com/{ID}/")
            embed.set_image(url=xkcd["img"])
            embed.set_footer(text=f"{months[int(xkcd['month'])-1]}, {xkcd['year']}")
            await ctx.send(embed=embed)
        else:
            await ctx.send("Invalid ID!")

def setup(bot):
    bot.add_cog(xkcd(bot))