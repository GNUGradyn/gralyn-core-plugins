from discord.ext import commands
import discord
import youtube_dl
from colorama import init, Fore, Back, Style
init()
import os
import json
import shutil
import asyncio

def log(text):
  print(Style.BRIGHT + Fore.WHITE + '[' + Fore.RED + 'Music' + Fore.WHITE + '] ' + Style.RESET_ALL + text)
lookup_opts = {
    "simulate": True,
    "quiet" : True, #TODO: make this part of config.json
}

if not os.path.exists("plugins/music-cache"):
  log("Creating cache..")
  os.makedirs("plugins/music-cache")

class Music(commands.Cog):
  def __init__(self, bot):
      self.bot = bot
  @commands.command()
  async def volume(self, ctx, level):
    if ctx.voice_client != None:
      ctx.voice_client.source.volume = float(level) / 100
      await ctx.message.add_reaction("✅")
    else: 
      await ctx.send("Not playing anything!")
  @commands.command()
  async def leave(self, ctx):
    """Leave the current channel"""
    await ctx.voice_client.disconnect()
    await ctx.message.add_reaction("✅")
  @commands.command(hidden=True)
  async def clearcache(self, ctx):
    """Clear the audio cache"""
    if ctx.author.id in ctx.bot.config["admins"]:
      log("Cache cleared!")
      await ctx.message.add_reaction("✅")
      shutil.rmtree("plugins/music-cache")
      os.makedirs("plugins/music-cache")
    else:
      await ctx.send(ctx.bot.denied())
  @commands.command()
  async def play(self, ctx, url):
    """Download and play a link from youtube"""
    if ctx.author.voice != None:
      message = await ctx.send(f"Downloading <{url}>..")
      with youtube_dl.YoutubeDL(lookup_opts) as ydl:
        try:
          info = ydl.extract_info(url)
          await message.edit(content=f"Downloading {info['title']}..")
        except:
          await ctx.send("An error occured downloading that video! Are you sure that URL is correct?")
        def callback(d):
          if d['status'] == 'finished':
            async def play(d):
              voice = await ctx.author.voice.channel.connect()
              source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(d["filename"]), volume=1.0) #TODO: Configurable default volume
              voice.play(source)
              await ctx.send(f"Playing {info['title']}...")
            self.bot.loop.create_task(play(d))
        download_opts = {
          "format": 'm4a',
          "quiet" : True, #TODO: make this part of config.json
          'progress_hooks': [callback],
          "outtmpl": "plugins/music-cache/%(id)s.m4a"
        }
        with youtube_dl.YoutubeDL(download_opts) as ydl:
            ydl.download([url])
    else:
        await ctx.send("You are not in a voice channel!")
def setup(bot):
    bot.add_cog(Music(bot))