id = 
guild_id = 
from discord.ext.commands import Cog
import discord
import time

def log(message):
    event = f"{time.strftime('%I:%M%p on %b %d, %Y')} - {message}"
    print(event)
    with open("presmon.txt", "a") as log_file:
        log_file.write(event + "\n")

class presmon(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.status = None
    @Cog.listener()
    async def on_ready(self):
        guild = self.bot.get_guild(guild_id)
        member = guild.get_member(id)
        self.status = member.status
        log(f"START STAT: {member.status}")
    @Cog.listener()
    async def on_member_update(self, before, after):
        if after.id == id:
            if after.status != self.status:
                self.status = after.status
                log(f"STAT CHANGE: {before.status}>{after.status}")
    @Cog.listener()
    async def on_message(self, message):
        if message.author.id == id:
            if message.author.status == discord.Status.offline or message.author.status == discord.Status.idle:
                log(f"FAKE {self.status}, MESSAGE IN {message.guild.name}")

def setup(bot):
    bot.add_cog(presmon(bot))